package com.sda.customerservice.controller;

import com.sda.customerservice.model.CustomerDto;
import com.sda.customerservice.service.CustomerDataPopulatorService;
import com.sda.customerservice.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/service/customers")
public class CustomerRestController {

    private final CustomerService customerService;
    private final CustomerDataPopulatorService customerDataPopulatorService;

    @GetMapping
    public ResponseEntity<List<CustomerDto>> getCustomers() {
        return ResponseEntity.ok().body(customerService.getCustomers());
    }

    @PostMapping
    public ResponseEntity<CustomerDto> saveCustomer(@RequestBody CustomerDto customerDto) {
        return ResponseEntity.ok().body(customerService.saveCustomer(customerDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerDto> updateCustomer(@PathVariable("id") final Long customerId, @RequestBody CustomerDto customerDto) {
        customerDto.setId(customerId);
        return ResponseEntity.ok().body(customerService.saveCustomer(customerDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CustomerDto> deleteCustomer(@PathVariable("id") final Long customerId) {
        customerService.deleteCustomerById(customerId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> getCustomerById(@PathVariable("id") final Long customerId) {
        final Optional<CustomerDto> optionalCustomerDto = customerDataPopulatorService.getCustomerById(customerId);
        if (optionalCustomerDto.isPresent()) {
            return ResponseEntity.ok().body(optionalCustomerDto.get());
        } else {
            return ResponseEntity.ok().body(null);
        }
    }

}

