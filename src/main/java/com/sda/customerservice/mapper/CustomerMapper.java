package com.sda.customerservice.mapper;

import com.sda.customerservice.entity.AddressEntity;
import com.sda.customerservice.entity.CustomerEntity;
import com.sda.customerservice.model.AddressDto;
import com.sda.customerservice.model.CustomerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class CustomerMapper {

    private final AddressMapper addressMapper;

    public CustomerDto map(final CustomerEntity customerEntity) {

        final List<AddressEntity> addressEntities = customerEntity.getAddressEntities();
        final List<AddressDto> addressDtos = new ArrayList<>();

        if (addressEntities != null && addressEntities.size() > 0) {
            addressDtos.addAll(addressEntities.stream().map(addressMapper::map).collect(Collectors.toList()));
        }

        return CustomerDto.builder().
                id(customerEntity.getId())
                .name(customerEntity.getName())
                .surname(customerEntity.getSurname())
                .age(customerEntity.getAge())
                .emailAddress(customerEntity.getEmailAddress())
                .monthlyIncome(customerEntity.getMonthlyIncome())
                .addressDtos(addressDtos)
                .userType(customerEntity.getUserType()).build();

    }

    public CustomerEntity map(final CustomerDto customerDto) {

        final List<AddressDto> addressDtos = customerDto.getAddressDtos();
        final List<AddressEntity> addressEntities = new ArrayList<>();

        if (addressDtos.size() > 0) {
            addressEntities.addAll(addressDtos.stream().map(addressMapper::map).collect(Collectors.toList()));
        }

        return CustomerEntity.builder().
                id(customerDto.getId())
                .name(customerDto.getName())
                .surname(customerDto.getSurname())
                .age(customerDto.getAge())
                .emailAddress(customerDto.getEmailAddress())
                .monthlyIncome(customerDto.getMonthlyIncome())
                .addressEntities(addressEntities)
                .userType(customerDto.getUserType()).build();

    }

    public CustomerEntity map(final CustomerEntity customerEntity, final CustomerDto customerDto) {

        customerEntity.setName(customerDto.getName());
        customerEntity.setSurname(customerDto.getSurname());
        customerEntity.setAge(customerDto.getAge());
        customerEntity.setEmailAddress(customerDto.getEmailAddress());
        customerEntity.setMonthlyIncome(customerDto.getMonthlyIncome());
        customerEntity.setUserType(customerDto.getUserType());

        return customerEntity;

    }

}
