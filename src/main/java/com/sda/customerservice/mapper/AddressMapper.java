package com.sda.customerservice.mapper;

import com.sda.customerservice.entity.AddressEntity;
import com.sda.customerservice.model.AddressDto;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public AddressDto map(final AddressEntity addressEntity) {
        return AddressDto.builder()
                .id(addressEntity.getId())
                .Street(addressEntity.getStreet())
                .city(addressEntity.getCity())
                .addressType(addressEntity.getAddressType())
                .country(addressEntity.getCountry()).build();
    }

    public AddressEntity map(final AddressDto addressDto) {
        return AddressEntity.builder()
                .id(addressDto.getId())
                .Street(addressDto.getStreet())
                .city(addressDto.getCity())
                .addressType(addressDto.getAddressType())
                .country(addressDto.getCountry()).build();
    }

}
