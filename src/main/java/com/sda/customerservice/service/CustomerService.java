package com.sda.customerservice.service;

import com.sda.customerservice.entity.AddressEntity;
import com.sda.customerservice.entity.CustomerEntity;
import com.sda.customerservice.mapper.CustomerMapper;
import com.sda.customerservice.model.CustomerDto;
import com.sda.customerservice.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerMapper customerMapper;
    private final CustomerRepository customerRepository;

    public List<CustomerDto> getCustomers() {
        final List<CustomerEntity> customerEntities = customerRepository.findAll();
        return customerEntities.stream().map(customerMapper::map).collect(Collectors.toList());
    }

    public CustomerDto getCustomerById(final Long customerId) {
        final CustomerEntity customerEntity = customerRepository.findById(customerId).orElseThrow(() -> new EntityNotFoundException("Customer not found with Id : " + customerId));
        return customerMapper.map(customerEntity);
    }

    public CustomerDto saveCustomer(final CustomerDto customerDto) {
        if (customerDto.getId() != null) {
            final CustomerEntity customerEntity = customerRepository.findById(customerDto.getId()).orElseThrow(() -> new RuntimeException("Customer not fond with Id : " + customerDto.getId()));
            final CustomerEntity updatedCustomerEntity = customerMapper.map(customerEntity, customerDto);
            customerRepository.save(updatedCustomerEntity);
            return customerMapper.map(updatedCustomerEntity);
        } else {
            final CustomerEntity customerEntity = customerMapper.map(customerDto);
            final List<AddressEntity> addressEntities = customerEntity.getAddressEntities().stream().map(addressEntity -> {
                addressEntity.setCustomer(customerEntity);
                return addressEntity;
            }).collect(Collectors.toList());
            customerEntity.setAddressEntities(addressEntities);
            final CustomerEntity savedCustomerEntity = customerRepository.save(customerEntity);
            return customerMapper.map(savedCustomerEntity);
        }
    }

    public void deleteCustomerById(final Long customerId) {
        customerRepository.deleteById(customerId);
    }
}
