package com.sda.customerservice.service;

import com.sda.customerservice.model.AddressDto;
import com.sda.customerservice.model.AddressType;
import com.sda.customerservice.model.CustomerDto;
import com.sda.customerservice.model.UserType;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerDataPopulatorService {


    public List<CustomerDto> getCustomers() {

        final List<CustomerDto> customers = new ArrayList<>();
        final AddressDto addressDto1 = AddressDto.builder().Street("Lastekodu 18").city("Tallinn").country("Estonia").addressType(AddressType.HOME).build();
        final AddressDto addressDto2 = AddressDto.builder().Street("Tartu mantee 10").city("Tallinn").country("Estonia").addressType(AddressType.OFFICE).build();
        final CustomerDto customerDto = CustomerDto.builder().id(1L).userType(UserType.PRIVATE).name("Isa").surname("Kalinsaz").age(30).emailAddress("isa.kalinsaz@bankish.eu").monthlyIncome(BigDecimal.valueOf(1000)).addressDtos(Arrays.asList(addressDto1, addressDto2)).build();
        customers.add(customerDto);

        final AddressDto addressDto3 = AddressDto.builder().Street("North of the king street").city("Tallinn").country("Estonia").addressType(AddressType.HOME).build();
        final AddressDto addressDto4 = AddressDto.builder().Street("The wall 1").city("Tallinn").country("Estonia").addressType(AddressType.OFFICE).build();
        final CustomerDto customerDto2 = CustomerDto.builder().id(2L).userType(UserType.BUSINESS).name("John").surname("snow").age(30).emailAddress("john.snow@bankish.eu").monthlyIncome(BigDecimal.valueOf(10000)).addressDtos(Arrays.asList(addressDto3, addressDto4)).build();
        customers.add(customerDto2);

        return customers;
    }

    public Optional<CustomerDto> getCustomerById(final Long customerId) {
        return getCustomers().stream().filter(customerDto -> customerId.equals(customerDto.getId())).findFirst();
    }

}
