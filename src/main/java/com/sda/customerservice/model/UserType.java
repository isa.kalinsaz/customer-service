package com.sda.customerservice.model;

public enum UserType {
    PRIVATE,
    BUSINESS
}
