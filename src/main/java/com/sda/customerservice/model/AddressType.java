package com.sda.customerservice.model;

public enum AddressType {
    HOME,
    OFFICE
}
