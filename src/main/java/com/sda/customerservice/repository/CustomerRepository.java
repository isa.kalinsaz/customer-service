package com.sda.customerservice.repository;

import com.sda.customerservice.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    @Query("select c from CustomerEntity c where c.emailAddress = :emailAddress")
    List<CustomerEntity> findByEmailAddressCustomQuery(@Param("emailAddress") final String emailAddress);

}
