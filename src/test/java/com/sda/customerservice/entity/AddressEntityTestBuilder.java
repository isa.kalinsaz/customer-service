package com.sda.customerservice.entity;

import static com.sda.customerservice.model.AddressType.HOME;

public class AddressEntityTestBuilder extends AddressEntity.AddressEntityBuilder {

    public static AddressEntityTestBuilder anAddress() {
        return new AddressEntityTestBuilder();
    }

    public AddressEntityTestBuilder withDefaultValues() {
        this.id(1L).addressType(HOME).Street("Parnu mantee 71").city("Tallinn").country("Estonia");
        return this;
    }

}