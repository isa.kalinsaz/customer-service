package com.sda.customerservice;

import com.sda.customerservice.model.CustomerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpEntity.EMPTY;
import static org.springframework.http.HttpMethod.GET;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class CustomerServiceApplicationIntegrationTests {

   /* @LocalServerPort
    private int port;*/

    @Value("${server.port}")
    private int port;

    @Value("${server.local.address}")
    private String serverLocalAddress;

    @Autowired
    private TestRestTemplate restTemplate;


    @BeforeEach
    void setUp() {

    }

    @Test
    void shouldGetCustomers() {
        // given,when
        final ResponseEntity<List<CustomerDto>> responseEntity = restTemplate.exchange(serverLocalAddress + ":" + port + "/api/service/customers", GET, EMPTY, new ParameterizedTypeReference<List<CustomerDto>>() {
        });

        // then
        assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(responseEntity.getBody().size()).isGreaterThan(0);
    }

    @Test
    void shouldGetCustomer() {
        // given,when
        final ResponseEntity<CustomerDto> responseEntity = restTemplate.getForEntity(serverLocalAddress + ":" + port + "/api/service/customers/1", CustomerDto.class);

        // then
        assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
        assertThat(responseEntity.getBody().getName()).isEqualTo("Isa");
        assertThat(responseEntity.getBody().getSurname()).isEqualTo("Kalinsaz");
    }

}
