package com.sda.customerservice.controller;

import com.sda.customerservice.model.CustomerDto;
import com.sda.customerservice.service.CustomerDataPopulatorService;
import com.sda.customerservice.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static java.util.Arrays.asList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = CustomerRestController.class)
public class CustomerRestControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CustomerService customerService;

    @MockBean
    private CustomerDataPopulatorService customerDataPopulatorService;

    @Test
    void shouldHaveSuccessfulResponseForGetCustomers() throws Exception {
        // given
        given(customerService.getCustomers()).willReturn(asList(CustomerDto.builder().build()));

        // when, then
        mockMvc.perform(MockMvcRequestBuilders.get("/api/service/customers")).andDo(print()).andExpect(status().is2xxSuccessful());
    }
}