package com.sda.customerservice.repository;

import com.sda.customerservice.entity.CustomerEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ExtendWith(SpringExtension.class)
public class CustomerRepositoryIntegrationTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void shouldFindCustomersByEmailAddress() {
        // given
        final String emailAddress = "isa.kalinsaz@bankish.eu";

        // when
        final List<CustomerEntity> customerEntities = customerRepository.findByEmailAddressCustomQuery(emailAddress);

        // then
        assertThat(customerEntities).isNotNull();
        assertThat(customerEntities.size()).isGreaterThan(0);

    }
}