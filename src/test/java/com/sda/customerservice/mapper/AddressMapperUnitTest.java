package com.sda.customerservice.mapper;

import com.sda.customerservice.entity.AddressEntity;
import com.sda.customerservice.model.AddressDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.sda.customerservice.entity.AddressEntityTestBuilder.anAddress;
import static com.sda.customerservice.model.AddressType.HOME;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class AddressMapperUnitTest {

    @InjectMocks
    private AddressMapper addressMapper;

    @Test
    void shouldMapFromEntityToDToWithNoValue() {
        // given
        final AddressEntity addressEntity = anAddress().withDefaultValues().build();

        // when
        final AddressDto actual = addressMapper.map(addressEntity);

        // then
        assertThat(actual).isNotNull();
    }

    @Test
    void shouldMapFromEntityToDToWithValue() {
        // given
        final AddressEntity addressEntity = anAddress().withDefaultValues().id(1L).addressType(HOME).Street("Lastekodu 18").city("Tallinn").country("Estonia").build();

        // when
        final AddressDto actual = addressMapper.map(addressEntity);

        // then
        assertThat(actual.getId()).isEqualTo(1L);
        assertThat(actual.getAddressType()).isEqualTo(HOME);
        assertThat(actual.getStreet()).isEqualTo("Lastekodu 18");
        assertThat(actual.getCity()).isEqualTo("Tallinn");
        assertThat(actual.getCountry()).isEqualTo("Estonia");
    }

}