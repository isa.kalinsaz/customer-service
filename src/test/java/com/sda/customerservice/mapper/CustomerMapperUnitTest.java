package com.sda.customerservice.mapper;

import com.sda.customerservice.entity.AddressEntity;
import com.sda.customerservice.entity.CustomerEntity;
import com.sda.customerservice.model.AddressDto;
import com.sda.customerservice.model.CustomerDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class CustomerMapperUnitTest {

    @Mock
    private AddressMapper addressMapper;

    @InjectMocks
    private CustomerMapper customerMapper;

    @BeforeEach
    void setUp() {
    }

    @Test
    void shouldMapFromEntityToDToWithNoValue() {
        // given
        final CustomerEntity customerEntity = CustomerEntity.builder().build();

        // when
        final CustomerDto actual = customerMapper.map(customerEntity);

        // then
        assertThat(actual).isNotNull();
    }

    @Test
    void shouldMapFromEntityToDToWithValue() {
        // given
        final CustomerEntity customerEntity = CustomerEntity.builder().id(1L).name("Isa").surname("Kalinsaz").age(30).build();

        // when
        final CustomerDto actual = customerMapper.map(customerEntity);

        // then
        assertThat(actual.getId()).isEqualTo(1L);
        assertThat(actual.getName()).isEqualTo("Isa");
        assertThat(actual.getSurname()).isEqualTo("Kalinsaz");
        assertThat(actual.getAge()).isEqualTo(30);
        assertThat(actual.getAddressDtos()).isNullOrEmpty();
    }

    @Test
    void shouldMapFromEntityToDToWithAddress() {
        // given
        final AddressEntity addressEntity = AddressEntity.builder().id(1L).build();
        final CustomerEntity customerEntity = CustomerEntity.builder().addressEntities(asList(addressEntity)).build();
        given(addressMapper.map(any(AddressEntity.class))).willReturn(AddressDto.builder().id(1L).build());

        // when
        final CustomerDto actual = customerMapper.map(customerEntity);

        // then
        assertThat(actual.getAddressDtos().size()).isEqualTo(1);
        assertThat(actual.getAddressDtos().get(0).getId()).isEqualTo(1L);
    }

}